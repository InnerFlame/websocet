<?php
require 'vendor/autoload.php';

use ChatApp\Models\Message;

$messages = Message::all();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <script src="assets/js/jquery-3.1.0.min.js"></script>
    <script src="assets/js/jquery.cookie.js"></script>
    <script src="assets/js/main.js"></script>
</head>
<body>
<div class="container">
    <ul class="message-list">
        <?php foreach($messages as $message): ?>
            <li><?= $message->text ?></li>
        <?php endforeach; ?>
    </ul>
    <form class="form-horizontal chat-form" method="post">
        <div class="form-group">
            <label for="message">Comment:</label>
            <textarea name="message" class="form-control" rows="5" id="message"></textarea>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>