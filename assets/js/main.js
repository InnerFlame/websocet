$(document).ready(function() {
        var conn = new WebSocket('ws://websocet:8080/');

        var chat_form = $('.chat-form'),
            chat_form_message = chat_form.find('#message');

        var message_list = $('.message-list');

        conn.onopen = function (e) {
            //$.ajax({
            //    url: '/history.php',
            //    dataType: 'json',
            //    success: function(data){
            //        $.each(data, function(){
            //            message_list.prepend('<li>' + this.text + '</li>');
            //        });
            //    }
            //});
        };

        conn.onmessage = function (e) {
            console.log(e.data);
            message_list.prepend('<li>' + e.data + '</li>');
        };

        chat_form.on('submit', function (e) {
            e.preventDefault();
            conn.send(chat_form_message.val());
            message_list.prepend('<li>' + chat_form_message.val() + '</li>');
        });
    }
);